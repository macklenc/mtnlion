FROM registry.gitlab.com/macklenc/mtnlion:devel as sde

RUN sudo apt-get update -y

########################
# Make your life better!
########################

# Better-than-bash shell
RUN sudo apt-get install -y zsh
RUN sudo apt-get install -y vim # make sure vim is updated

# Much-more-betterer-than-bash shell
RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" &&\
    sed -i 's/ZSH_THEME.*/ZSH_THEME="af-magic"/' ${HOME}/.zshrc &&\
    sed -i 's/plugins=.*/plugins=(git common-aliases extract pip sudo)/' ${HOME}/.zshrc

# Install npm and rust (rust is required for exa)
RUN sudo apt-get install -y npm
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y &&\
    echo "export PATH=~/.cargo/bin:$PATH" >> ${HOME}/.zshrc

# Bat is a pretty version of cat
RUN wget --output-document=bat.deb https://github.com/sharkdp/bat/releases/download/v0.12.1/bat_0.12.1_amd64.deb &&\
    sudo dpkg -i bat.deb &&\
    rm bat.deb &&\
    echo 'alias cat="bat"' >> ${HOME}/.zshrc

# diff-so-fancy makes diffs look much better, tldr is handy for man pages
# RUN sudo npm install -g diff-so-fancy tldr
RUN git config --global core.pager "diff-so-fancy | less --tabs=4 -RFX"
RUN echo ' \n\
function dsf () { \n\
    git diff --no-index --color "$@" | diff-so-fancy | less --tabs=4 -RFX \n\
} \n\
' >> ${HOME}/.zshrc

# exa is a very colorful version of ls
RUN ${HOME}/.cargo/bin/cargo install exa &&\
    echo 'alias ls="exa"' >> ${HOME}/.zshrc &&\
    echo 'alias l="exa -lahF"' >> ${HOME}/.zshrc

# Get much more out of vim
RUN git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime &&\
    sh ~/.vim_runtime/install_awesome_vimrc.sh &&\
    echo 'export EDITOR=vim' >> ${HOME}/.zshrc

################################################################################################################
# The following packages require that you can forward X11. It may be worth trying a browser-based IDE like theia
################################################################################################################

# Install gvim
#RUN sudo apt-get install -y vim-gtk3

# Install sublime
#RUN wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add - &&\
#    sudo apt-get install -y apt-transport-https &&\
#    echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list &&\
#    sudo apt-get update &&\
#    sudo apt-get install -y sublime-text sublime-merge

# Install pycharm
#RUN wget https://download.jetbrains.com/python/pycharm-community-2019.3.tar.gz -qO - | sudo tar xfz - -C /opt/ &&\
#    cd /usr/bin &&\
#    sudo ln -s /opt/pycharm-*/bin/pycharm.sh pycharm

ENTRYPOINT ["/bin/zsh"]
