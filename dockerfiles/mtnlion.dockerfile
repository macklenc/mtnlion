# ############################################
# Update the FEniCS image with the latest refs
# ############################################
FROM quay.io/fenicsproject/stable:2018.1.0.r3 AS base

# System dependencies
RUN apt-get update -y && apt-get install -y python3-tk

# Create mtnlion user
RUN usermod -u 10000 fenics &&\
    groupmod -g 10000 fenics
RUN groupadd -g 1000 mtnlion &&\
    useradd --create-home --shell /bin/bash -u 1000 --gid users --groups sudo,docker_env,fenics mtnlion
RUN echo "mtnlion ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
WORKDIR /home/mtnlion

# python dependencies
USER mtnlion
ENV PATH="/home/mtnlion/.local/bin:${PATH}"
RUN pip3 install --upgrade --user pip pipenv==2021.5.29 setuptools

ENTRYPOINT ["/bin/bash"]

# ###############################
# Create the mtnlion:latest image
# ###############################
FROM base AS release

# Install mtnlion
USER mtnlion
ADD --chown=mtnlion:mtnlion . /tmp/mtnlion
RUN (cd /tmp/mtnlion && PIP_USER=1 pipenv install --system --deploy && python3 setup.py install --user)
RUN rm -rf mtnlion

# ##############################
# Create the mtnlion:devel image
# ##############################
FROM base AS dev_base

# Dependencies
RUN sudo apt-get install -y git-flow clang-tidy clang-format

# Install mtnlion
ADD --chown=mtnlion:mtnlion . /home/mtnlion/mtnlion

WORKDIR /home/mtnlion/mtnlion
RUN pipenv --python 3.6 --site-packages &&\
    pipenv sync --dev

# Install pre-commit hooks
RUN pipenv run pre-commit install
