FROM quay.io/fenicsproject/dev-env:2018.1.0 as fenics

# Setup FEniCS source code
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
USER fenics
WORKDIR /home/fenics
ENV FENICS_VERSION=2018.1.0
ENV PATH=$PATH:/home/fenics/bin
ENV FENICS_BUILD_TYPE='Debug'
RUN sed -i 's/\(^ *make$\)/\1 -j/' bin/fenics-build
RUN cat bin/fenics-build && cat bin/fenics-update
RUN /bin/bash -c ". fenics.env.conf; env; fenics-update"
RUN sudo chmod g+w -R ../fenics

from fenics as devel

USER root

# Dependencies
RUN apt-get update -y &&\
    apt-get install -y python3-tk git git-flow python3.6-dev clang-tidy clang-format

# Shell
RUN apt-get install -y zsh

# Nice shell
RUN wget -O .zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc &&\
    wget -O .zshrc.local  https://git.grml.org/f/grml-etc-core/etc/skel/.zshrc

# Install mtnlion
ENV PATH=$PATH:/home/mtnlion/.local/bin
RUN git clone https://gitlab.com/macklenc/mtnlion.git
RUN pip install -r mtnlion/requirements.txt --upgrade --user
RUN pip install -r mtnlion/docs/requirements.txt --upgrade --user
RUN cd mtnlion && python3 setup.py develop --user && cd ..

# Install pre-commit hooks
RUN cd mtnlion &&\
    pre-commit install &&\
    cd ..

WORKDIR /home/mtnlion
ENTRYPOINT ["/bin/zsh"]
