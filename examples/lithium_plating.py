import dolfin as fem
import matplotlib.pyplot as plt
import numpy as np

from examples.preprocessor import preprocessor, run
from mtnlion import report
from mtnlion.cell import P2D
from mtnlion.domain import Domain
from mtnlion.formula import Formula
from mtnlion.models import LithiumPlating
from mtnlion.problem_space import ProblemSpace, ProblemSpaceAssembler
from mtnlion.rothes import Euler


def sim(raw_mesh, start_time, dt, stop_time, Iapp):
    dtc = fem.Constant(dt, name="dt")
    params, consts = preprocessor("lithium_plating")

    print("Initializing Model...")
    Ns = 8

    params_dict = {
        **{k: v for k, v in params.items() if k not in ["Uocp"]},
        **{k: v for k, v in consts.items() if k not in ["kappa_ref"]},
        "Mp": Domain({"anode": 7.3e4, "cathode": 7.3e4}),
        "rho_p": Domain({"anode": 2.1e3, "cathode": 2.1e3}),
        "kappa_p": Domain({"anode": 1, "cathode": 1}),
        "Rsei": Domain({"anode": 0.01, "cathode": 0}),
        "Uref_oc": 0,
        "Iapp": fem.Constant(0, name="Iapp"),
    }

    class DomainMeasure(Formula):
        def __init__(self, dx):
            super(DomainMeasure, self).__init__(name="dx", domains=["anode", "separator", "cathode"])
            self.dx = dx

        def form(self, arguments, domain):
            return self.dx

    domain = P2D(raw_mesh)
    model = LithiumPlating(Ns)
    model.add_formula(DomainMeasure(domain.domain_data.domain_measure))
    problem_space = (
        ProblemSpace(model, domain, Euler(dtc))
        .assign_elements(domain.create_element("CG", 1), domains=["anode", "separator", "cathode"])
        .assign_elements(
            domain.create_element("R", 0), domains=["anode_cc", "cathode_cc", "anode-separator", "separator-cathode"]
        )
        .assign_elements(domain.create_element("CG", 2), names=("j_s"))
        .generate_variables()
    )

    model_assembler = ProblemSpaceAssembler(problem_space, params_dict)

    solutions = run(
        params,
        consts,
        start_time,
        dt,
        stop_time,
        params_dict["Iapp"],
        model_assembler,
        Iapp,
        domain.create_function_space(domain.create_element("CG", 1)),
        save_functions=["phi_s", "phi_e", "c_e", "j", "cse", "j_s"],
    )

    return solutions


def main(start_time=None, dt=None, stop_time=None, plot_time=None):
    fem.set_log_level(fem.LogLevel.ERROR)
    fem.parameters["form_compiler"]["optimize"] = True
    fem.parameters["form_compiler"]["cpp_optimize"] = True
    fem.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3"

    # Times at which to run solver
    if start_time is None:
        start_time = 0
    if stop_time is None:
        stop_time = 30
    if dt is None:
        dt = 0.1
    if plot_time is None:
        plot_time = np.arange(start_time, stop_time, (stop_time - start_time) / 10)

    mesh = np.linspace(0, 1, 20)
    solutions = sim(mesh, start_time, dt, stop_time, lambda t: -41 if 5 <= t <= 10 else 0)
    _report = report.Report(solutions, plot_time, split=False)

    plt.plot(solutions.time, solutions.solutions["phi_s"]["cathode"][:, -1])
    plt.show()

    _report.plot()


if __name__ == "__main__":
    main()
