import dolfin as fem
import matplotlib.pyplot as plt
import numpy as np

from examples.preprocessor import preprocessor, run
from mtnlion import report
from mtnlion.cell import P2D
from mtnlion.domain import Domain
from mtnlion.formula import Formula
from mtnlion.models import Thermal
from mtnlion.problem_space import ProblemSpace, ProblemSpaceAssembler
from mtnlion.rothes import Euler


class Convection(Formula):
    def __init__(self):
        super(Convection, self).__init__(domains=["anode_cc", "cathode_cc"])

        self.Variables = self.typedef("Variables", "T")
        self.Parameters = self.typedef("Parameters", "hboundary, Tref")

    def form(self, arguments, domain):
        (T,) = arguments.variables
        hboundary, Tref = arguments.parameters

        return -hboundary * (Tref - T) * T.test()


class dUocp(Formula):
    def __init__(self):
        super(dUocp, self).__init__(name="dUocp", domains=["anode", "cathode"])

        self.Parameters = self.typedef("Parameters", "soc")

    def form(self, arguments, domain):
        (soc,) = arguments.parameters

        if domain == "anode":
            return (
                0.00036229929 * soc ** 2
                - 0.0008520278805 * soc
                + 0.0002698001697
                + 1408.08992029081 * fem.exp(-32.9633287 * soc) / (1 + 5422044.61436694 * fem.exp(-34.79099646 * soc))
            )
        else:
            return (
                0.0127660158 * soc ** 3
                - 0.026064581 * soc ** 2
                + 0.008147113434 * soc
                + 0.00431274309 * fem.exp(0.571536523 * soc)
                - 0.001281681122 * fem.sin(4.9916739 * soc)
                + 9.0453431e-5 * fem.sin(20.9669665 * soc - 12.578825)
                - 3.13472974e-5 * fem.sin(31.7663338 * soc - 22.4295664)
                - 0.00414532933
                - 0.000184274863 * fem.exp(-466.834808419646 * (soc - 0.5169435168) ** 2)
            )


def sim(raw_mesh, start_time, dt, stop_time, Iapp):
    dtc = fem.Constant(dt, name="dt")
    params, consts = preprocessor("sei")

    print("Initializing Model...")
    Ns = 8

    SOC0 = 0.6
    theta_init = params["theta0"] + SOC0 * (params["theta100"] - params["theta0"])
    params["cs_0"] = params["csmax"] * theta_init

    params_dict = {
        **{k: v for k, v in params.items() if k not in ["Uocp"]},
        **{k: v for k, v in consts.items() if k not in ["kappa_ref"]},
        "rho": Domain({"anode": 2500, "separator": 1200, "cathode": 1500}),
        "cp": Domain({"anode": 700, "separator": 700, "cathode": 700}),
        "lambd": Domain({"anode": 5, "separator": 1, "cathode": 5}),
        "hboundary": 5,
        "Iapp": fem.Constant(0, name="Iapp"),
    }

    domain = P2D(raw_mesh)
    model = Thermal(Ns)
    model.add_formula(dUocp())
    model.add_formula(Convection())
    problem_space = (
        ProblemSpace(model, domain, Euler(dtc))
        .assign_elements(domain.create_element("CG", 1), domains=["anode", "separator", "cathode"])
        .assign_elements(
            domain.create_element("R", 0), domains=["anode_cc", "cathode_cc", "anode-separator", "separator-cathode"]
        )
        .generate_variables()
    )

    model_assembler = ProblemSpaceAssembler(problem_space, params_dict)

    parameters = {
        "absolute_tolerance": 1e-6,
        "relative_tolerance": 1e-6,
    }

    solutions = run(
        params,
        consts,
        start_time,
        dt,
        stop_time,
        params_dict["Iapp"],
        model_assembler,
        Iapp,
        domain.create_function_space(domain.create_element("CG", 1)),
        save_functions=["phi_s", "phi_e", "c_e", "j", "cse", "dUocp", "q", "T", "qi", "qr", "qs", "qe1", "qe2"],
        parameters=parameters,
    )

    return solutions


def main(start_time=None, dt=None, stop_time=None, plot_time=None):
    fem.set_log_level(fem.LogLevel.ERROR)
    fem.parameters["form_compiler"]["optimize"] = True
    fem.parameters["form_compiler"]["cpp_optimize"] = True
    fem.parameters["form_compiler"]["cpp_optimize_flags"] = "-O3"

    # Times at which to run solver
    if start_time is None:
        start_time = 0
    if stop_time is None:
        stop_time = 15
    if dt is None:
        dt = 0.1
    if plot_time is None:
        plot_time = np.arange(start_time, stop_time, (stop_time - start_time) / 10)

    mesh = np.linspace(0, 1, 20)
    solutions = sim(
        mesh, start_time, dt, stop_time, lambda t: -20.5 if 5 <= t <= 10 else (-20.5 if 30 <= t <= 40 else 0)
    )
    _report = report.Report(solutions, plot_time, split=False)

    plt.plot(solutions.time, solutions.solutions["phi_s"]["cathode"][:, -1])
    plt.show()

    _report.plot()


if __name__ == "__main__":
    main()
