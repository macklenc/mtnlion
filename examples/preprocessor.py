import collections.abc
import json
import numbers
import pathlib

import dolfin as fem
import numpy as np
import sympy as sym

from mtnlion import domain
from mtnlion import solution as solution
from mtnlion.formulas import dfn
from mtnlion.problem_space import ProblemSpaceAssembler
from mtnlion.tools.cache import CACHE_DIR


@domain.eval_domain("auto", pass_domain=True)
def fenics_constantify(v, name, domain):
    return fem.Constant(v, name="{}[{}]".format(name, domain)) if isinstance(v, numbers.Number) else v


def fenics_domain(placement):
    mesh = fem.IntervalMesh(len(placement) - 1, 0, 3)
    mesh.coordinates()[:] = np.array([placement]).transpose()

    P1 = fem.FiniteElement("CG", mesh.ufl_cell(), 1)
    R0 = fem.FiniteElement("R", mesh.ufl_cell(), 0)

    V = fem.FunctionSpace(mesh, P1)

    class Left(fem.SubDomain):
        def inside(self, x, on_boundary):
            return x[0] < 0.0 + fem.DOLFIN_EPS and on_boundary

    class Right(fem.SubDomain):
        def inside(self, x, on_boundary):
            return x[0] > 1.0 - fem.DOLFIN_EPS and on_boundary

    boundaries = fem.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    boundaries.set_all(2)

    left = Left()
    right = Right()
    left.mark(boundaries, 0)
    right.mark(boundaries, 1)

    dx = fem.Measure("dx", domain=mesh)
    ds = fem.Measure("ds", domain=mesh, subdomain_data=boundaries)

    return mesh, V, P1, R0, dx, ds


def param_preprocessor(raw_params):
    params = {k: domain.Domain(v) if isinstance(v, dict) else v for k, v in raw_params["params"].items()}
    consts = {k: domain.Domain(v) if isinstance(v, dict) else v for k, v in raw_params["consts"].items()}

    def kappa_D(R, T, F, t_plus, dfdc, **kwargs):
        kd = 2 * R * T / F * (1 + dfdc) * (t_plus - 1)

        return kd

    tmpx, soc = sym.symbols("x soc")
    params["Uocp"]["anode"] = sym.sympify(params["Uocp"]["anode"]).subs(tmpx, soc)
    params["Uocp"]["cathode"] = sym.sympify(params["Uocp"]["cathode"]).subs(tmpx, soc)
    params["De_eff"] = consts["De_ref"] * params["eps_e"] ** params["brug_De"]
    params["sigma_eff"] = params["sigma_ref"] * params["eps_s"] ** params["brug_sigma"]
    params["a_s"] = 3 * params["eps_s"] / params["Rs"]
    params["a_s"]["separator"] = 0
    params["cs_0"] = params["csmax"] * params["theta100"]  # set_domain_data(0.325, 0.422)

    consts["F"] = 96487
    consts["T"] = 298.15
    consts["R"] = 8.314
    consts["dfdc"] = 0
    consts["kappa_D"] = kappa_D(**params, **consts)

    return params, consts


def update(d, u):
    for k, v in u.items():
        d[k] = update(d.get(k, {}), v) if isinstance(v, collections.abc.Mapping) else v
    return d


def preprocessor(overrides=None):
    CACHE_DIR.mkdir(parents=True, exist_ok=True)
    params_file: pathlib.Path = pathlib.Path(__file__).parent / "params.json"
    raw_parameters = json.loads(params_file.read_text())

    if overrides is not None:
        overrides_file = pathlib.Path(__file__).parent / f"{overrides}.json"
        update(raw_parameters, json.loads(overrides_file.read_text()))

    params, consts = param_preprocessor(raw_parameters)
    fenics_params = {k: fenics_constantify(v, k) for k, v in params.items()}
    fenics_consts = {k: fenics_constantify(v, k) for k, v in consts.items()}

    return fenics_params, fenics_consts


def run(
    params,
    consts,
    start_time,
    dt,
    stop_time,
    Iapp,
    assembled_model: ProblemSpaceAssembler,
    input_current,
    V,
    save_functions=None,
    parameters=None,
):
    time = np.arange(start_time, stop_time, dt)

    F = assembled_model.formulate()

    if save_functions is None:
        save_functions = []

    solutions = solution.Solution(assembled_model, save_functions, V)
    solutions.set_solution_time_steps(len(time))

    print("Loading problem space...")
    J = fem.derivative(
        F, assembled_model.problem_space.function_manager.u_vec[0], assembled_model.problem_space.function_manager.trial
    )
    problem_solver = fem.NonlinearVariationalProblem(F, assembled_model.problem_space.function_manager.u_vec[0], J=J)
    solver = fem.NonlinearVariationalSolver(problem_solver)

    prm = solver.parameters

    prm["newton_solver"]["absolute_tolerance"] = 1e-8
    prm["newton_solver"]["relative_tolerance"] = 1e-7
    prm["newton_solver"]["maximum_iterations"] = 100
    prm["newton_solver"]["relaxation_parameter"] = 1.0

    if parameters is not None:
        prm["newton_solver"].update(parameters)

    print("Initializing...")
    Uocp_init = dfn.eval_form(dfn.Uocp(params["Uocp"]), params["theta100"])
    phis_init = Uocp_init["cathode"] - Uocp_init["anode"]

    init = {
        "c_s": {"anode": [params["cs_0"]["anode"]], "cathode": [params["cs_0"]["cathode"]]},
        "phi_s": {"anode": [fem.Constant(0)], "cathode": [phis_init]},
        "phi_e": {"anode": [-Uocp_init["anode"]]},
        "c_e": {"anode": [consts["ce0"]], "separator": [consts["ce0"]], "cathode": [consts["ce0"]]},
        "T": {"anode": [consts["T"]], "separator": [consts["T"]], "cathode": [consts["T"]]},
    }

    for variable, domains in init.items():
        for _domain, value in domains.items():
            try:
                if isinstance(value[0], fem.Constant):
                    assembled_model.problem_space.function_manager.initialize_from_constant(variable, value, _domain)
                else:
                    assembled_model.problem_space.function_manager.initialize_from_expression(variable, value, _domain)
            except KeyError:
                pass

    assembled_model.problem_space.function_manager.u_vec[1].assign(
        assembled_model.problem_space.function_manager.u_vec[0]
    )

    print("Starting simulation.")
    for k, t in enumerate(time):
        Iapp.assign(input_current(t))

        iterations, converged = solver.solve()
        assembled_model.problem_space.function_manager.u_vec[1].assign(
            assembled_model.problem_space.function_manager.u_vec[0]
        )

        print("t={time:.3f}: num iterations: {iter}".format(time=t, iter=iterations))

        solutions.save_solution(k, t)

    return solutions
