# Feature description

Provide a brief summary of what the feature is/does.

## Use-cases

How will this feature be used?

* Who/what are the actors?
* Are there any preconditions/postconditions?
* What is the main success scenario?
* Are there any extensions (other scenarios and error cases)?

# Benefits

Who does this benefit?

# Requirements

What are the requirements for this feature?

# Links and References

Optional

Choose relevant labels:

/label ~feature
/cc @macklenc
<!-- /assign @macklenc -->

