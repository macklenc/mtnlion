=====
Usage
=====

To use Mountian Lion CSS in a project::

    import mtnlion

Examples can be found in the examples directory of the project `repository <https://gitlab.com/macklenc/mtnlion>`_. 