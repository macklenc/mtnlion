import os

import pytest

from mtnlion import model
from tests.unit.mock import MockFormula, MockVariable


@pytest.fixture
def mk_model():
    formulas = (
        MockFormula("1", ["anode", "cathode"], ["1", "3"]),
        MockFormula("2", ["anode", "separator", "cathode"], ["2"]),
        MockFormula("3", ["cathode"], ["1", "2", "3"]),
        MockFormula("4", ["anode"], ["1", "4"]),
    )

    variables = [
        MockVariable("1", ["anode", "cathode"], 1),
        MockVariable("2", ["anode", "separator", "cathode"], 2),
        MockVariable("3", ["cathode"], 3),
        MockVariable("4", ["anode"], 1),
    ]

    mod = model.Model(minimum_rothes_order=1)
    mod.formulas = formulas
    mod.variables = variables

    return mod


class TestModel:
    def test_check(self, mk_model):
        # Everything should pass
        mk_model.check()

        # Test missing variable
        mk_model.variables[2].name = "5"
        with pytest.raises(LookupError):
            mk_model.check()
        mk_model.variables[2].name = "3"

        # Test unused variable, should warn
        mk_model.formulas[3].variables[1] = "2"
        mk_model.check()
        mk_model.formulas[3].variables[1] = "4"

        # Test formula name uniqueness
        mk_model.formulas[2].name = "1"
        with pytest.raises(ReferenceError):
            mk_model.check()
        mk_model.formulas[2].name = "3"

        # Test variable name uniqueness
        mk_model.variables.append(MockVariable("1", ("anode",), 3))
        with pytest.raises(ReferenceError):
            mk_model.check()
        mk_model.variables = mk_model.variables[:-1]

    def test_domains(self, mk_model):
        assert mk_model.domains == ("anode", "cathode", "separator")
        mk_model.variables.append(MockVariable("tst", ("anode_cc",), 2))
        assert mk_model.domains == ("anode", "anode_cc", "cathode", "separator")

    def test_variable_by_name(self, mk_model):
        assert mk_model.variable_by_name("1") == mk_model.variables[0]

        with pytest.raises(KeyError):
            mk_model.variable_by_name("asdf")

    def test_rename_variable(self, mk_model):
        tmp_variable = mk_model.variable_by_name("1")
        mk_model.rename_variable("1", "6")
        assert mk_model.variable_by_name("6") == tmp_variable
        assert mk_model.formulas[0].variables[0] == "6"
        assert mk_model.formulas[3].variables[0] == "6"
        mk_model.rename_variable("6", "1")

        with pytest.raises(KeyError):
            mk_model.rename_variable("6", "1")

    def test_formula_by_name(self, mk_model):
        assert mk_model.formula_by_name("1") == mk_model.formulas[0]

        with pytest.raises(KeyError):
            mk_model.formula_by_name("asdf")

    def test_add_formula(self, mk_model):
        tst_form = MockFormula("test", ("cathode_cc",), ("2", "3"))
        mk_model.add_formula(tst_form)

        assert mk_model.formula_by_name("test") == tst_form

    def test_replace_formula(self, mk_model):
        tst_form = MockFormula("2", ("cathode_cc",), ("2", "3"))
        mk_model.replace_formulas(tst_form)
        assert mk_model.formula_by_name("2") == tst_form

        tst_form = MockFormula("asdf", ("cathode_cc",), ("2", "3"))
        with pytest.raises(KeyError):
            mk_model.replace_formulas(tst_form)

    def test_exclude_formula(self, mk_model):
        mk_model.exclude_formulas("2")

        with pytest.raises(KeyError):
            mk_model.formula_by_name("2")


if __name__ == "__main__":
    pytest.main(args=["-s", os.path.abspath(__file__)])
