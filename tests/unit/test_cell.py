import os

import pytest

from mtnlion import cell
from mtnlion.domain import Domain
from tests.unit.mock import MockDomain, MockDomainData, MockMeasureMap, MockMesh


class TestDomainInterface:
    def test_create(self):
        di = MockDomain(5)
        assert di.mesh_grid == 5
        assert di.domains == Domain()
        assert di.domain_data == MockDomainData(MockMesh(5), 1, 2)

    def test_set_domain_measure(self, monkeypatch):
        monkeypatch.setattr(cell, "MeasureMap", MockMeasureMap)
        di = MockDomain(4)
        di.add_domain_measure("anode", 10, 2)
        assert di.domains["anode"] == [MockMeasureMap(10, 2, None)]
        di.add_domain_measure("anode", 4, 4)
        assert di.domains["anode"] == [MockMeasureMap(10, 2, None), MockMeasureMap(4, 4, None)]
        di.add_domain_measure("cathode", 3, 5)
        assert di.domains["cathode"] == [MockMeasureMap(3, 5, None)]

    def test_boundary_of(self, monkeypatch):
        monkeypatch.setattr(cell, "MeasureMap", MockMeasureMap)
        di = MockDomain(5)
        di.boundary_of("anode_cc", ["anode"], [3], [1])
        assert di.domains["anode_cc"] == [MockMeasureMap(3, 1, "anode")]
        di.boundary_of("anode-separator", ["anode", "separator"], [1, 2], [3, 4])
        assert di.domains["anode-separator"] == [MockMeasureMap(1, 3, "anode"), MockMeasureMap(2, 4, "separator")]

    def test_is_boundary(self, monkeypatch):
        monkeypatch.setattr(cell, "MeasureMap", MockMeasureMap)
        di = MockDomain(5)
        di.add_domain_measure("anode", 10, 2)
        di.boundary_of("anode_cc", ["anode"], [3], [1])
        assert di.is_boundary("anode") is False
        assert di.is_boundary("anode_cc") is True

    def test_create_element(self, monkeypatch):
        di = MockDomain(5)
        monkeypatch.setattr(cell.fem, "FiniteElement", lambda x, y, z: (x, y, z))
        assert di.create_element("test", 5) == ("test", 5, 5)

    def test_create_function_space(self, monkeypatch):
        di = MockDomain(5)
        monkeypatch.setattr(cell.fem, "FunctionSpace", lambda x, y: (x, y))
        assert di.create_function_space(5) == (di.domain_data.mesh, 5)


if __name__ == "__main__":
    pytest.main(args=["-s", os.path.abspath(__file__)])
