from collections import namedtuple

from mtnlion.cell import DomainInterface


class MockType:
    def __init__(self, name, variables):
        self.name = name
        self.variables = variables

    def __name__(self):
        return self.name


class MockFormula:
    def __init__(self, name, domains, variables):
        self.name = name
        self.domains = domains
        self.variables = variables
        self.Variables = MockType("", "")
        self.Parameters = MockType("", "")

    def typedef(self, name, arguments):
        self.variables = list(arguments)
        return name


class MockVariable:
    def __init__(self, name, domains, num_functions):
        self.name = name
        self.domains = domains
        self.num_functions = num_functions

    def __eq__(self, other):
        return self.name == other.name and self.domains == other.domains and self.num_functions == other.num_functions


class MockTD:
    def __init__(self, order):
        self.order = order

    def __call__(self, *args, **kwargs):
        return 0

    def __eq__(self, other):
        return self.order == other.order


MockDomainData = namedtuple("MockDomainData", "mesh, domain_measure, boundary_measure")
MockMeasureMap = namedtuple("MockMeasureMap", "measure, multiple, primary_domain")


class MockMesh:
    def __init__(self, data):
        self.data = data

    def ufl_cell(self):
        return self.data

    def __eq__(self, other):
        return self.data == other.data


class MockDomain(DomainInterface):
    def __init__(self, grid):
        super(MockDomain, self).__init__(grid)

    def _domain_data(self):
        return MockDomainData(MockMesh(self.mesh_grid), 1, 2)
