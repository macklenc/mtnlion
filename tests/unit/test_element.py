import os

import pytest

from mtnlion import domain, element


@pytest.fixture
def equation_spec():
    return {
        "1": domain.Domain({"anode": [1], "cathode": [6]}),
        "2": domain.Domain({"anode": [2], "cathode": [7, 8]}),
        "3": domain.Domain({"anode": [3, 4], "cathode": [9, 10]}),
        "4": domain.Domain({"anode": [5], "cathode": [11, 12]}),
    }


class TestElementSpace:
    def test_map(self, equation_spec):
        es = element.ElementSpace(equation_spec)
        assert es.mapping == {
            "1": {"anode": [0], "cathode": [1]},
            "2": {"anode": [2], "cathode": [3, 4]},
            "3": {"anode": [5, 6], "cathode": [7, 8]},
            "4": {"anode": [9], "cathode": [10, 11]},
        }
        assert all(
            i in es.elements for domains in equation_spec.values() for values in domains.values() for i in values
        )

        # test reused elements
        equation_spec["4"]["cathode"][1] = 1
        es = element.ElementSpace(equation_spec)
        assert es.elements.count(1) == 2  # TODO: elements should be deduplicated

        assert len(es.elements) == len(es)

    def test_shift(self, equation_spec):
        es = element.ElementSpace(equation_spec)
        es.shift(3)
        assert es.mapping["1"]["anode"] == [3]


class TestMixedElementSpace:
    def test_creation(self, equation_spec):
        space1 = element.ElementSpace(equation_spec)
        eq_spec2 = {
            "10": domain.Domain({"anode": [13], "cathode": [18]}),
            "11": domain.Domain({"anode": [14], "cathode": [19, 20]}),
            "12": domain.Domain({"anode": [15, 16], "cathode": [21, 22]}),
            "13": domain.Domain({"anode": [17], "cathode": [23, 24]}),
        }
        space2 = element.ElementSpace(eq_spec2)
        mixed = element.MixedElementSpace(space1, space2)

        assert space1.mapping == {key: value for key, value in mixed.mapping.items() if key in space1.mapping}
        assert mixed.mapping["10"]["anode"] == [12]
        assert mixed.elements[12] == 13


if __name__ == "__main__":
    pytest.main(args=["-s", os.path.abspath(__file__)])
