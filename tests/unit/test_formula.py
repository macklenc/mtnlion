import os
from collections import namedtuple

import pytest

from mtnlion.domain import Domain
from mtnlion.formula import Arguments, FormMap, Formula
from tests.unit.mock import MockFormula, MockMeasureMap, MockTD, MockVariable


@pytest.fixture
def mvars():
    return [
        MockVariable("1", ["anode", "cathode"], 1),
        MockVariable("2", ["anode", "separator", "cathode"], 2),
        MockVariable("3", ["cathode"], 3),
        MockVariable("4", ["anode"], 1),
    ]


@pytest.fixture
def mforms():
    return [
        MockFormula("1", ["anode", "cathode"], ["1", "3"]),
        MockFormula("2", ["anode", "separator", "cathode"], ["2"]),
        MockFormula("3", ["cathode"], ["1", "2", "3"]),
        MockFormula("4", ["anode"], ["1", "4"]),
    ]


@pytest.fixture
def mparams():
    return [
        Domain({"anode": 0, "separator": 1, "cathode": 2}),
        Domain({"anode": 3, "cathode": 4}),
        Domain({"anode": 5}),
        Domain({"anode": 6, "separator": 7, "cathode": 8}),
    ]


@pytest.fixture
def mtd():
    return [
        MockTD(2),
    ]


@pytest.fixture
def marg(mvars, mforms, mparams, mtd):
    arguments = Arguments(mvars, mparams, (), mtd)
    Variables = namedtuple("Variables", "w, x, y, z")
    Parameters = namedtuple("Params", "a, b, c, d")
    TimeD = namedtuple("TD", "t")

    arguments.assign_argument_classes(Variables, Parameters, namedtuple("L", ""), TimeD)

    return arguments


@pytest.fixture
def mfmap():
    form = Former()
    measure = MockMeasureMap(0, 1, 2)

    return FormMap(form, measure, "anode")


class TestArguments:
    def test_convert(self, marg):
        marg.convert()

        vars = namedtuple("Variables", "w, y, z")
        tmp = marg._classes["variables"]
        marg._classes["variables"] = vars
        with pytest.raises(TypeError):
            marg.convert()
        marg._classes["variables"] = tmp

    def test_pops(self, marg):
        marg.convert()
        w, x = marg.pop_variables(("w", "x"))
        assert w.name == "1"
        assert x.name == "2"

        with pytest.raises(KeyError):
            (x,) = marg.pop_variables("x")

        a, b = marg.pop_parameters(("a", "b"))
        assert a == Domain({"anode": 0, "separator": 1, "cathode": 2})
        assert b == Domain({"anode": 3, "cathode": 4})

        (td,) = marg.pop_time_derivatives(("t",))
        assert td == MockTD(2)


class Former(Formula):
    def __init__(self):
        super(Former, self).__init__("tester", ["anode", "cathode"])
        self.Variables = self.typedef("Variables", "w, x, y, z")
        self.Parameters = self.typedef("Params", "a, b, c, d")
        self.TimeDiscretization = self.typedef("TD", "t")

    def form(self, arguments: Arguments, domain: str):
        return (*arguments.variables, *arguments.parameters, *arguments.time_discretization)


@pytest.fixture
def mform():
    return Former()


class TestFormula:
    def test_properties(self, mform):
        assert mform.variables == ("w", "x", "y", "z")
        assert mform.parameters == ("a", "b", "c", "d")
        assert mform.time_discretizations == ("t",)

    def test_formulate(self, mform, marg, mvars, mparams, mtd):
        assert mform.formulate(marg, "anode") == (*mvars, *mparams, *mtd)

    def test_append(self, mform):
        assert mform.append_arguments(mform.Variables, "s, d, f".split(", "))._fields == (
            "w",
            "x",
            "y",
            "z",
            "s",
            "d",
            "f",
        )


class TestFormMap:
    def test_properties(self, mfmap):
        assert mfmap.name == Former().name
        mfmap.name = "test"
        assert mfmap.name == "test"
        assert mfmap.domains == Former().domains
        assert mfmap.variables == Former().variables
        assert mfmap.primary_domain == 2
        assert repr(mfmap) == "test[2 -> anode]: unformulated"


if __name__ == "__main__":
    pytest.main(args=["-s", os.path.abspath(__file__)])
