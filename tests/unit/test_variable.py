import os

import pytest

from mtnlion import variable


@pytest.fixture
def mk_variable():
    return variable.Variable("test", ("anode", "cathode"), 1)


class TestCheckSingle:
    def test_no_arg(self, mk_variable):
        tst_operation = variable.check_single(lambda x: x)

        with pytest.raises(variable.SingleDomainError):
            tst_operation(mk_variable)

    def test_multi_domain(self, mk_variable):
        tst_operation = variable.check_single(lambda x, y: (x, y))

        with pytest.raises(variable.SingleDomainError):
            tst_operation(mk_variable.get_domain("anode"), mk_variable)

    def test_variable_operation(self, mk_variable):
        tst_operation = variable.check_single(lambda x, y: (x, y))
        tst_var = variable.Variable("test2", ("anode", "cathode"), 1)

        result = tst_operation(mk_variable.get_domain("anode"), tst_var.get_domain("anode"))
        assert isinstance(result[0], variable.Variable)
        assert isinstance(result[1], variable.UndefinedDomain)

    def test_scalar_operation(self, mk_variable):
        integer = 6
        tst_operation = variable.check_single(lambda x, y: (x, y))

        result = tst_operation(mk_variable.get_domain("anode"), integer)
        assert isinstance(result[0], variable.Variable)
        assert isinstance(result[1], int)


class TestVariable:
    def test_creation(self):
        tst_var = variable.Variable("tst", ("anode", "separator", "cathode"), 5)
        assert tst_var.domains == ("anode", "separator", "cathode")
        assert tst_var.name == "tst"
        assert tst_var.num_functions == 5
        assert len(tst_var.uv_map.element_map["anode"]) == 5
        assert len(tst_var.uv_map.test_function["anode"]) == 5
        assert len(tst_var.uv_map.trial_function[0]["anode"]) == 5

    def test_defined(self, mk_variable):
        tst_variable = variable.Variable("tst", ("anode", "cathode"), 2)

        assert not mk_variable.is_defined
        mk_variable.uv_map.element_map["anode"][0] = True
        assert not mk_variable.is_defined
        mk_variable.uv_map.element_map["cathode"][0] = True
        assert mk_variable.is_defined

        assert not tst_variable.is_defined
        tst_variable.uv_map.element_map["anode"][0] = True
        assert not tst_variable.is_defined
        tst_variable.uv_map.element_map["cathode"][0] = True
        assert not tst_variable.is_defined
        tst_variable.uv_map.element_map["anode"][1] = True
        assert not tst_variable.is_defined
        tst_variable.uv_map.element_map["cathode"][1] = True
        assert tst_variable.is_defined

    def test_get_domain(self, mk_variable):
        tst_domain = mk_variable.get_domain("anode")

        assert tst_domain.domains == ["anode"]
        assert not tst_domain.is_defined
        tst_domain.uv_map.element_map[0] = True
        assert tst_domain.is_defined

        assert tst_domain.name == mk_variable.name
        assert isinstance(tst_domain.uv_map, variable.UVMapSingleDomain)

        assert tst_domain.get_domain("anode") == tst_domain
        assert not tst_domain.get_domain("cathode").is_defined

    def test_trial(self):
        tst_var = variable.Variable("test", ("anode", "cathode"), 2)
        tst_var.uv_map.trial_function[0]["anode"][0] = 0
        tst_var.uv_map.trial_function[0]["anode"][1] = 1

        with pytest.raises(variable.SingleDomainError):
            tst_var.trial()

        tst_var_domain = tst_var.get_domain("anode")
        trial = tst_var_domain.trial(all_funcs=True)
        assert trial == [[0, 1]]

        trial = tst_var_domain.trial(history=0, subfunction=0)
        assert trial == 0
        trial = tst_var_domain.trial(history=0, subfunction=1)
        assert trial == 1

        trial = tst_var_domain.trial(history=0, all_funcs=True)
        assert trial == [0, 1]

        trial = tst_var_domain.trial(subfunction=0)
        assert trial == [0]
        trial = tst_var_domain.trial(subfunction=1)
        assert trial == [1]

    def test_test(self):
        tst_var = variable.Variable("test", ("anode", "cathode"), 2)
        tst_var.uv_map.test_function["anode"][0] = 0
        tst_var.uv_map.test_function["anode"][1] = 1

        with pytest.raises(variable.SingleDomainError):
            tst_var.test()

        tst_var_domain = tst_var.get_domain("anode")

        test = tst_var_domain.test(subfunction=0)
        assert test == 0
        test = tst_var_domain.test(subfunction=1)
        assert test == 1

        test = tst_var_domain.test(all_funcs=True)
        assert test == [0, 1]

    def test_equal(self, mk_variable):
        assert mk_variable == mk_variable

        with pytest.raises(TypeError):
            assert mk_variable == 0

    def test_iterable(self):
        tst_var = variable.Variable("test", ("anode", "cathode"), 2)
        tst_var.uv_map.trial_function[0]["anode"][0] = 0
        tst_var.uv_map.trial_function[0]["anode"][1] = 1

        with pytest.raises(variable.SingleDomainError):
            assert sum(tst_var) == 1

        assert sum(tst_var.get_domain("anode")) == 1


if __name__ == "__main__":
    pytest.main(args=["-s", os.path.abspath(__file__)])
