#!/bin/bash

set -e
echo "Verifying import sort correctness...."

exit_code=0
PROJECT_ROOT="$(git rev-parse --show-toplevel)"
while read line; do
	if [ $(echo $line | grep -c "Skipped") -eq 0 ]; then
		exit_code=1
	fi
	echo "$line"
done < <(isort -rc "$PROJECT_ROOT" --settings-path "$PROJECT_ROOT" --skip docs --skip venv 2>&1)

if [ $exit_code -eq 0 ]; then
	echo "Imports are in the correct order"
fi

exit $exit_code
