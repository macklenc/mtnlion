#!/bin/bash

set -e
echo "Checking for binaries...."

exit_code=0
while read line; do
	exit_code=1
	echo "$line"	
done < <(comm -13 <(git grep -Il '' -- $(git rev-parse --show-toplevel) | sort -u) <(git grep -al '' -- $(git rev-parse --show-toplevel) | sort -u) 2>&1)

if [ $exit_code -eq 0 ]; then
	echo "No binaries found"
fi

exit $exit_code
