#!/bin/bash

CFG_DIR="$HOME/.cache"
DOCKERFILE_PATH="$HOME/mtnlion-docker"
DOCKERFILE_HASH="$CFG_DIR/sde.md5"
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
DEFAULT_DOCKERFILE="${SCRIPTPATH}/../dockerfiles/mtnlion-development.dockerfile"
THEIA="${SCRIPTPATH}/../dockerfiles/theia"

# Make the prints distinct
GREEN='\033[0;32m'
NC='\033[0m'

function help {
    echo "$0 - Build/Run/Attach to SDE"
    echo ""
    echo "$0 [OPTIONS] "
    echo ""
    echo "If no OPTIONS are provided, $0 will build if required, and run the image using bash as the shell."
    echo ""
    echo "OPTIONS:"
    echo "-h, --help    show this help"
    echo "-s, --shell   set default shell (persistent)"
    echo "-b, --build   force build image"
}

function shell_cfg {
    local SHELL_CFG="$CFG_DIR/default_shell"

    if [ ! -z $1 ]; then
        echo $1 | tee "$SHELL_CFG"
    elif [ -e "$SHELL_CFG" ]; then
        cat "$SHELL_CFG"
    else
        echo "bash" | tee "$SHELL_CFG"
    fi
}

# arg 1: dockerfile
# arg 2: shell
function generate_dockerfile {
    local SHELL=$2
    mkdir -p "$DOCKERFILE_PATH"
    cp "$DEFAULT_DOCKERFILE" "$1"
}

# arg 1: shell
function config {
    local DOCKERFILE="$DOCKERFILE_PATH/Dockerfile"
    local IMAGE=$(docker images -q mtnlion-sde:latest)
    local SHELL=$1

    # Create template Dockerfile if it doesn't exist
    if [ ! -e "$DOCKERFILE" ]; then
        echo -e "${GREEN}$DOCKERFILE doesn't exist, creating template${NC}"
        generate_dockerfile "$DOCKERFILE" "$SHELL"
        unset IMAGE
    fi

    # if md5 hash exists, check if Dockerfile changed, otherwise add the hash
    if [ -e "$DOCKERFILE_HASH" ]; then
        md5sum -c < "$DOCKERFILE_HASH" > /dev/null
        if [ $? -ne 0 ]; then
            echo -e "${GREEN}Dockerfile changed since last run.${NC}"
            return 1 # rebuild
        fi
    else
        md5sum "$DOCKERFILE_PATH/Dockerfile" > "$DOCKERFILE_HASH"
    fi

    if [ -z "$IMAGE" ]; then
        return 1 # build if there's no image
    fi

    return 0 # No rebuild required
}

function build {
    echo -e "${GREEN}Building SDE${NC}"
    DOCKER_BUILDKIT=1 docker build -t mtnlion-sde:latest "$DOCKERFILE_PATH"
    if [ $? -ne 0 ]; then
        exit $?
    fi
    md5sum "$DOCKERFILE_PATH/Dockerfile" > "$DOCKERFILE_HASH"
    echo -e "${GREEN}SDE build complete${NC}"
}

# arg 1: shell
function run {
    local LINES=$(stty size | awk '{print $1}') # set terminal size
    local COLUMNS=$(stty size | awk '{print $2}') # set terminal size
    local CONTAINER=$(docker ps | grep mtnlion-sde:latest | awk '{print $1}')
    local SHELL=$1

    if [ -z "$CONTAINER" ]; then
        XSOCK=/tmp/.X11-unix
        XAUTH=/tmp/.docker.xauth
        xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge - 2>/dev/null
        chmod 644 $XAUTH
        echo -e "${GREEN}No SDE running for mtnlion-sde, launching new container${NC}"
        docker run --rm --name=mtnlion-sde --net=host --env="DISPLAY" --privileged --volume mtnlion-sde:/home/mtnlion --volume $XSOCK:$XSOCK --volume $XAUTH:$XAUTH -e XAUTHORITY=$XAUTH -it mtnlion-sde:latest
    else
        echo -e "${GREEN}Found running SDE for mtnlion, attaching with current terminal settings${NC}"
        docker exec --env="DISPLAY" -it "$CONTAINER" "${SHELL[@]}" -c "stty cols $COLUMNS rows $LINES && $SHELL"
    fi
}

if [ $# -gt 0 ]; then
    while [ $# -gt 0 ]; do
    key="$1"
    case $key in
        -s|--shell)
        SHELL=$(shell_cfg $2)
        shift # past argument
        ;;
        -b|--build)
        BUILD=1
        ;;
        -h|--help)
        help
        exit 0
        ;;
        *)
        help
        exit 0
        ;;
    esac
    shift # past argument or value
    done
else
    echo "" # not needed at this point
fi

if [ ! -z $SHELL ]; then
    config "$(shell_cfg $1)"
else
    config "$SHELL"
fi

if [ $? -eq 1 ] || [ ! -z $BUILD ]; then
    build
fi

run "$(shell_cfg)"
