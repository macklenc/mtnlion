# -*- coding: utf-8 -*-

"""Top-level package for Mountian Lion CSS."""

import pkg_resources

__author__ = """Christopher Macklen"""
__email__ = "cmacklen@uccs.edu"
__version__ = pkg_resources.require("mtnlion")[0].version
