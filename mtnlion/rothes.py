"""
Rothe's method based time-stepping schemes.
"""

import abc
from typing import List

from ufl.indexed import Indexed  # type: ignore

# pylint: disable=invalid-name,too-few-public-methods


class Rothes:
    """
    Base class for defining Rothe's based time-stepping schemes.
    """

    def __init__(self, order: int, dt: float):
        """
        Create a Rothe's based time-stepping scheme.

        :param order: the order of the method
        :param dt: the time step size
        """
        self.order = order
        self.dt = dt

    @abc.abstractmethod
    def __call__(self, x: List[Indexed]):
        """
        Override this method with the implementation of the time-stepping scheme.

        :param x: List of asdfaf
        :return:
        """


class Euler(Rothes):
    """
    First order time-stepping method.
    """

    def __init__(self, dt):
        super(Euler, self).__init__(order=1, dt=dt)

    def __call__(self, x):
        return (x[0] - x[1]) / self.dt
