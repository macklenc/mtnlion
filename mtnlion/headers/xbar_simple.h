//
// Copyright (c) 2019 Christopher Macklen. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.
//

#ifndef MTNLION_HEADERS_XBAR_SIMPLE_H_
#define MTNLION_HEADERS_XBAR_SIMPLE_H_

#include <memory>

class XBarSimple : public Expression {
 public:
    XBarSimple() : Expression() {}

    void eval(Eigen::Ref<Eigen::VectorXd>& values, Eigen::Ref<const Eigen::VectorXd>& x, const ufc::cell& c) const {
        xbar->eval(values, x, c);
    }

    std::shared_ptr<GenericFunction> xbar;
};
#endif  // MTNLION_HEADERS_XBAR_SIMPLE_H_
