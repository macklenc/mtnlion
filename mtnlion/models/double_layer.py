"""
Isothermal model extended with double-layer capacitance
"""
from mtnlion.formula import Formula, Variable
from mtnlion.models.isothermal import Isothermal

# pylint: disable=invalid-name


class DoubleLayer(Isothermal):
    """
    The double-layer model comes from surface science where a structure forms on the surface of a solid when exposed to
    a fluid. In this case, the structure that forms on the particles is an electrical charge which causes an opposing
    charge to build up in the electrolyte near the surface. The separation of electrical charge around the particle
    surface has the same behavior as a plate capacitor.
    """

    def __init__(self, Ns):
        super(DoubleLayer, self).__init__(Ns)
        self.rename_variable("j", "j_f")

        self.variables += [Variable("j", ["anode", "cathode"]), Variable("j_dl", ["anode", "cathode"])]

        self.formulas += [self.TotalFlux(), self.DoubleLayer()]

    class TotalFlux(Formula):
        """
        Replaces the standard intercalation flux from the isothermal model.
        """

        def __init__(self):
            super(DoubleLayer.TotalFlux, self).__init__(domains=["anode", "cathode"])
            self.Variables = self.typedef("Variables", "j, j_f, j_dl")

        def form(self, arguments, domain):
            j, jf, jdl = arguments.variables

            return (j - (jf + jdl)) * j.test()

    class DoubleLayer(Formula):
        """
        Double-layer flux.
        """

        def __init__(self):
            super(DoubleLayer.DoubleLayer, self).__init__(domains=["anode", "cathode"])
            self.Variables = self.typedef("Variables", "j_dl, phi_s, phi_e")
            self.Parameters = self.typedef("Parameters", "Cdl, Rdl, F")
            self.TimeDiscretization = self.typedef("TimeDiscretization", "dt_j_dl, dt_phi_s, dt_phi_e")

        def form(self, arguments, domain):
            jdl, phis, phie = arguments.variables
            dt_jdl, dt_phis, dt_phie = arguments.time_discretization
            Cdl, Rdl, F = arguments.parameters

            lhs = Cdl * (dt_phis(phis.trial()) - dt_phie(phie.trial())) / F
            rhs = Rdl * Cdl * dt_jdl(jdl.trial()) + jdl

            return (lhs - rhs) * jdl.test()
