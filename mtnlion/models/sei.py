"""
Isothermal model extended with SEI layer growth
"""
import dolfin as fem

from mtnlion.formula import Formula, Variable
from mtnlion.formulas.dfn import CapacityLoss, FilmResistance, FilmThickness
from mtnlion.models.isothermal import Isothermal

# pylint: disable=invalid-name


class SEI(Isothermal):
    """
    The SEI model is an extension to the isothermal model that attempts to quantify solid-electrolyte interphase
    formation and growth on the negative-electrode solid particles during chargning.
    """

    def __init__(self, Ns):
        super(SEI, self).__init__(Ns)

        self.variables += [Variable("j_s", ["anode"]), Variable("delta_film", ["anode"]), Variable("Q", ["anode"])]

        self.formulas += [
            self.SideReactionFlux(),
            self.SideReactionOverpotential(),
            FilmThickness(),
            FilmResistance(),
            CapacityLoss(),
            self.LocalMolecularFlux(),
        ]

    class Overpotential(Formula):
        """
        Voltage difference between a reduction potential and the potential of the redox event.
        """

        def __init__(self):
            super(SEI.Overpotential, self).__init__(name="eta", domains=["anode", "cathode"])
            self.Variables = self.typedef("Variables", "phi_s, phi_e, j")
            self.Parameters = self.typedef("Parameters", "F, Uocp, Rfilm, j_total")

        def form(self, arguments, domain):  # pylint: disable=R1710
            phis, phie, j = arguments.variables
            (F, Uocp, Rfilm, j_total) = arguments.parameters

            if domain == "anode":
                return phis - phie - Uocp - F * Rfilm * j_total

            if domain == "cathode":
                Rfilm = 0
                return phis - phie - Uocp - F * Rfilm * j

    class LocalMolecularFlux(Formula):
        """
        The total flux of the system including intercalation flux and side reaction flux.
        """

        def __init__(self):
            super(SEI.LocalMolecularFlux, self).__init__(name="j_total", domains=["anode"])
            self.Variables = self.typedef("Variables", "j, j_s")

        def form(self, arguments, domain):
            j, js = arguments.variables

            return j + js

    class SideReactionFlux(Formula):
        """
        Describes how the electrical current on an electrode depends on the electrode potential due to the side
        reaction.
        """

        def __init__(self):
            super(SEI.SideReactionFlux, self).__init__(domains=["anode"])
            self.Variables = self.typedef("Variables", "j_s")
            self.Parameters = self.typedef("Parameters", "io_sei, alpha, F, R, T, eta_s")

        def form(self, arguments, domain):
            """Flux through the boundary of the solid."""
            (js,) = arguments.variables
            io_sei, alpha, F, R, T, eta_s = arguments.parameters

            return (js + io_sei / F * fem.exp(-alpha * F * eta_s / (R * T))) * js.test()

    class SideReactionOverpotential(Formula):
        """
        Voltage difference between a reduction potential and the potential of the redox event in the side reaction.
        """

        def __init__(self):
            super(SEI.SideReactionOverpotential, self).__init__(name="eta_s", domains=["anode"])
            self.Variables = self.typedef("Variables", "phi_s, phi_e")
            self.Parameters = self.typedef("Parameters", "Uref_s, F, Rfilm, j_total")

        def form(self, arguments, domain):
            phis, phie = arguments.variables
            Uref_sei, F, Rfilm, j_total = arguments.parameters

            return phis - phie - Uref_sei - F * Rfilm * j_total
